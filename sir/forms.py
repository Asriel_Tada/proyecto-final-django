from django import forms
from django.forms import widgets
from .models import *


class NewPost(forms.Form):
    
    title = forms.CharField(max_length=150)
    imagen = forms.ImageField()
    text = forms.CharField(widget=forms.Textarea)
    file_name = forms.CharField(max_length=200)
    file_description = forms.CharField(widget=forms.Textarea)
    file_post = forms.FileField()

    
    def save(self):
        if self.cleaned_data.get('file_name') != None and self.cleaned_data.get('file_description') != None and self.cleaned_data.get('file_post') != None :
            file_add = File_Post(file_name=self.cleaned_data.get('file_name') , file_description=self.cleaned_data.get('file_description') , file_post=self.cleaned_data.get('file_post') )
            file_add.save()
            post_add = Post(file_in = file_add,title=self.cleaned_data.get('title') , imagen=self.cleaned_data.get('imagen') , text=self.cleaned_data.get('text') )
            return post_add
        else:
            post_add = Post(title=self.cleaned_data.get('title') , imagen=self.cleaned_data.get('imagen') , text=self.cleaned_data.get('text') )
            return post_add



class NewHilo(forms.ModelForm):
    imagen_name = forms.CharField(max_length=200)
    imagen_description = forms.CharField(widget=forms.Textarea)
    imagen_file = forms.ImageField()


class NewNewFile(forms.ModelForm):
    class Meta:
        model = File_Post
        fields = '__all__'
        labels = {
            'file_name': 'Nombre del Archivo',
            'file_description': 'Descripcion del archivo',
            'file_post': 'Asocia El archivo',
        }



class NewNewPost(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ('file_in','author',)
        labels = {
            'title': 'Titulo del post',
            'imagen': 'Asocia una imagen al post',
            'text': 'Explayate como quieras',
            
        }


class NewNewForum(forms.ModelForm):
    class Meta:
        model = Forum
        fields = '__all__'

class NewNewHilo(forms.ModelForm):
    class Meta:
        model = Hilo
        exclude = ["author"] #se exluye el campo autor
        #fields = '__all__'
        labels = {
            'imagen_name': 'Titulo del comentario',
            'imagen_description': 'Comentario',
            'imagen_file': 'Asocia una imagen',
        }
