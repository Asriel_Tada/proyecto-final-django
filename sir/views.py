from django.shortcuts import render , redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView
from .models import*
from .forms import *
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def Add_Post(request):
    formA= NewNewPost(request.POST,request.FILES or None)
    formB= NewNewFile(request.POST,request.FILES or None)
    template = 'acciones/Add_Post.html'
    if formA.is_valid():
        form_p = formA.save(commit=False)
        form_q = formB.save()
        form_p.author = get_object_or_404(User,username=request.user.username)
        form_p.file_in =form_q
        form_p.save()
        return redirect('Sir:Index_post')
    context = {"formA": formA,"formB": formB}
    return render(request, template, context)
@login_required
def Index_post(request):
    template = 'acciones/IndexPost.html'
    post_in_page = Post.objects.all()
    context = {"post_in_page": post_in_page}
    return render(request, template, context)

def View_Post(request, pk):
    template = 'acciones/View_Post.html'
    Posts =get_object_or_404(Post, pk=pk)
    print(Posts.title)
    context = {"Posts": Posts}
    return render(request, template, context)
@login_required
def Edit_Post(request, pk):
    template = 'acciones/form.html'
    Posts =get_object_or_404(Post, pk=pk)
    form= NewNewPost(request.POST or None, instance = Posts)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('Sir:Index_Forums')
    context = {"form": form}
    return render(request, template, context)
@login_required
def Delete_Post(request, pk):
    template = 'acciones/delete.html'
    Posts =get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        Posts.delete()
        return redirect('sir:Index_post')
    context = {"Post": Post}
    return render(request, template, context)



# Manejo del foro

@login_required
def Add_Forums(request):
    formA= NewNewPost(request.POST,request.FILES or None)
    formB= NewNewFile(request.POST,request.FILES or None)
    template = 'acciones/Add_Forum.html'
    if formA.is_valid():
        form_p = formA.save(commit=False)
        form_q = formB.save()
        form_p.author = get_object_or_404(User,username=request.user.username)
        form_p.file_in =form_q
        form_p.save()
        forum = Forum(post_in_forum=form_p)
        forum.save()
        return redirect('Sir:Index_Forums_Owner')
    context = {"formA": formA,"formB": formB,}
    return render(request, template, context)

def Index_Forums(request):
    template = 'acciones/IndexForum.html'
    post_in = Forum.objects.all()
    print (post_in)
    Posts=[]
    for a in post_in:
        b  =get_object_or_404(Post, pk=a.post_in_forum.id)
        Posts.append(b)
        print (Posts)
   

    context = {"post_in": post_in,"Posts":Posts}
    return render(request, template, context)
@login_required
def Index_Forums_Owner(request):
    template = 'acciones/Index_Forum_Owner.html'
    post_in = Forum.objects.all()
    print (post_in)
    author = User.objects.get(username=request.user.username)
    Posts=[]
    for a in post_in:
        b  = get_object_or_404(Post, pk=a.post_in_forum.id)
        if b.author == author:
            Posts.append(b)
            print (Posts)
        else:
            continue

    context = {"post_in": post_in,"Posts":Posts}
    return render(request, template, context)

def View_Forums(request, pk):
    template = 'acciones/View_Forum.html'
    Posts =get_object_or_404(Post, pk=pk)
    hilos =Forum.objects.get(post_in_forum=Posts)
    addhilo = NewNewHilo(request.POST,request.FILES or None)
    if hilos == None:
        add_void_hilos=Hilo(imagen_name=None,imagen_description=None,imagen_file=None)
        forum = Forum(post_in_forum=Posts)
        forum.save()
        forum.hilo_in_forum.add(add_void_hilos)
        print (forum)
    if addhilo.is_valid():
        FiHilo = addhilo.save(commit = False)
        FiHilo.author = request.user #el autor es el usuario que está actualmente registrado 
        FiHilo.save()
        hilos.hilo_in_forum.add(FiHilo)
        return redirect('/Sir/View_Forums/%s'% pk )
    print (hilos.hilo_in_forum)
    context = {"Posts": Posts,"hilos":hilos.hilo_in_forum,"addhilo":addhilo}
    return render(request, template, context)

@login_required
def Delete_Forums(request, pk):
    template = 'acciones/delete.html'
    Forums =get_object_or_404(Forum, pk=pk)
    if request.method == 'POST':
        Forums.delete()
        return redirect('Sir:Index_Forums')
    context = {"Forums": Forums}
    return render(request, template, context)
@login_required
def Edit_File(request, pk):
    template = 'acciones/file_form.html'
    Posts =get_object_or_404(Post, pk=pk)
    form= NewNewFile(request.POST or None, instance = Posts.file_in)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('Sir:Index_Forums')
    context = {"form": form}
    return render(request, template, context)