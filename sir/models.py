from django.db import models
from django.contrib.auth.models import User
from datetime import * 

# Create your models here.

class File_Post(models.Model):
    file_name = models.CharField(max_length=200,blank=True)
    file_description = models.TextField(blank=True)
    file_post = models.FileField(upload_to='Files',blank=True)

class Hilo(models.Model):
    date = models.DateTimeField(auto_now_add = True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null = True, blank = True)
    #imagen_name = models.CharField(max_length=200)
    imagen_description = models.TextField()
    imagen_file = models.ImageField(upload_to='Image')

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=150)
    imagen = models.ImageField(upload_to='Image_post')
    text = models.TextField()
    file_in = models.ForeignKey(File_Post,on_delete=models.CASCADE,blank=True)


    
class Forum(models.Model):
    post_in_forum= models.OneToOneField(Post, on_delete=models.CASCADE)
    hilo_in_forum = models.ManyToManyField(Hilo)

      

