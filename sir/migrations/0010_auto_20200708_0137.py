# Generated by Django 3.0.4 on 2020-07-08 01:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sir', '0009_remove_hilo_imagen_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hilo',
            name='imagen_file',
            field=models.ImageField(upload_to='Image'),
        ),
        migrations.AlterField(
            model_name='post',
            name='imagen',
            field=models.ImageField(upload_to='Image_post'),
        ),
    ]
