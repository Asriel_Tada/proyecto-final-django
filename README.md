Sistema de Foros
================

Proyecto T2
-----------

Este proyecto está basado en un sitema de foros donde el usuario que visite la página pordrá ver y/o comentar temas de interés. 

Tendrá distintas secciones con respectiva información donde el usuario en cuestión podrá acceder mediante el botón ir. Si el usuario no se encuentra registrado no podrá comentar en los foros, pero si podrá ver la información.

Instrucciones:

- Para entrar a la aplicación web ingrese a: http://127.0.0.1:8000/

- Para que el usuario pueda registrarse debe dirgirse a la esquina superior derecha y hacer click en "Registro", o si bien en algún foro ir a la parte finar y clickear el botón de registro para comentar. Esto redirigirá a la página de resgistro con sus respectivos campos de datos personales.

- En la sección de registro no se permiten contraseñas que sean demasiado comunes, como secuencias numéricas o palabras como "hola123". También deberá tener mínimo ocho caracteres.

- Para que el usuario pueda iniciar sesión debe dirigirse a la esquina superior derecha y hacer click en Inicio de sesión y colocar el nombre de usuario y contraseña respectivos.
- Usuarios para iniciar sesión:
	- Name 1: root
	- Pass 1: Proyecto1
    
    - Name 2: test
    - Pass 2: topicosdos

- El usuario tendrá distintos temas de Foros para visualizar y comentar, con sus respectivos titulos, para entrar a ellos debe hacer click en el boton "IR" del foro respectivo.

- En el foro se podrá ver primero el titulo de este, luego el autor, fecha e imagen publicada, finalmente la descripción de la imagen o del foro para finalizar con un link que permite descargar el archivo.

- En el campo de comentarios, el usuario solo podrá comentar si está registrado y se mostrará la fecha y hora en la que realizó el comentario junto con su nombre de usuario.

- Cuando se quiera volver a la página de inicio estando dentro de un foro, el usuario deberá dirigirse a la esquina superior derecha y presionar el botón "Foros" o el Logo.

- El usuario tiene un propio perfil, el cual puede editar si lo desea. Para esto debe dirigirse a la esquina superior derecha y hacer click en "Mi Perfil", aquí apareceran los campos de datos personales que el usuario quiera modificar o agregar.

- El usuario tendrá la opción de cambiar su contraseña cuyo botón se encuentra en la esquina superior derecha que redirigirá a los campos para completar dicha información a modificar.


- Si el usuario desea cerrar sesión debe dirigirse a la esquina superior derecha y hacer click en "Cerrar Sesión"

